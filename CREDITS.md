# RF-Track Credits

RF-Track uses Open Source components. You can find the source code of their open source projects along with license information below.  We acknowledge and are grateful to these developers for their contributions to open source.

**GSL - GNU Scientific Library** 
http://www.gnu.org/software/gsl/<br/>
Copyright (c) 2009 Free Software Foundation, Inc.<br/>
License: GNU General Public License https://www.gnu.org/licenses/gpl-3.0.html

**FFTW**, the "Fastest Fourier Transform in the West." https://www.fftw.org/<br/>
Copyright (c) 2003, 2007-11 Matteo Frigo,
Copyright (c) 2003, 2007-11 Massachusetts Institute of Technology.<br/>
License: GNU General Public License version 2 or any later version.
https://www.fftw.org/fftw3_doc/License-and-Copyright.html

**SWIG**, the Simplified Wrapper and Interface Generator https://www.swig.org/<br/>
Copyright (c) 1995-2011 The SWIG Developers<br/>
License: SWIG License https://www.swig.org/Release/LICENSE

RF-Track uses Octave and Python as user interfaces:

**Octave**, the Scientific Programming Language https://octave.org/<br/>
Copyright (c) 1987-1988, 1991-1997, 2003-2018 Free Software Foundation<br/>
Licence: GNU General Public License https://www.gnu.org/licenses/gpl-3.0.html

**Python**, the high-level general-purpose programming language https://www.python.org/<br/>
Copyright (c) 1991-1995 by Stichting Mathematisch Centrum, Amsterdam, The Netherlands<br/>
License: Python Software Foundation (PSF) License Agreement
https://docs.python.org/3/license.html
